from flask import Flask, render_template, request
from utils import *
from models import *


app = Flask(__name__)


@app.route("/")
def tabella():
    teams_raw = db.query(Team.id, Team.name, Team.points, Team.founded).filter(Team.ACTIVE == 1).order_by(Team.points.desc()).all()
    teams = [{"id": x[0], "name": x[1], "points": x[2], "founded": x[3]} for x in teams_raw]
    return render_template("index.html", teams=teams)

@app.route("/teams/new", methods=["GET", "POST"])
def team_new():
    if request.method == "POST":
        if request.form["team_name"]:
            if not db.query(Team.name).filter(Team.name == request.form["team_name"]).one_or_none():
                new_team = Team(
                    name=request.form["team_name"]
                )
                if request.form["captain"]:
                    new_team.captain = request.form["captain"]

                if request.form["founded"]:
                    new_team.founded = request.form["founded"]
                db.add(new_team)
                db.commit()
                return render_template("team_form.html", message="New team added to database")
            return render_template("team_form.html", message="Team name already taken")
        return render_template("team_form.html", message="Form is missing some values")
    return render_template("team_form.html")

@app.route("/teams/<id>")
def team_page(id):
    team = db.query(Team).filter(Team.id == id).one_or_none()
    if team:
        matches_raw = db.query(Match).filter(
            or_(Match.home_id == id,
            Match.guest_id == id)
            ).all()
        return render_template("team_page.html", team=team, matches=matches_raw)
    return "Team not found"

@app.route("/matches/")
def matches_list():
    matches_raw = db.query(Match).order_by(Match.CREATEDON.desc()).all()
    return render_template("matches.html",matches=matches_raw)

@app.route("/matches/new", methods=["GET", "POST"])
def new_match():
    teams = db.query(Team).filter(Team.ACTIVE == 1).order_by(Team.name).all()
    if request.method == "POST":
        if request.form["home_team"] and request.form["guest_team"] and request.form["home_score"] and request.form["guest_score"]:
            if request.form["home_team"] != request.form["guest_team"]:
                if int(request.form["home_score"]) >= 0 and int(request.form["guest_score"]) >= 0:
                    new_match = Match(
                        home_id=request.form["home_team"], 
                        guest_id=request.form["guest_team"], 
                        home_score=request.form["home_score"],
                        guest_score=request.form["guest_score"],
                    )

                    #Determining winner and calculating scores
                    if new_match.home_score > new_match.guest_score:
                        calculate_points(new_match.home_id, POINTS_FOR["victory"])
                        calculate_points(new_match.guest_id, POINTS_FOR["defeat"])
                        new_match.winner = 1

                    elif new_match.home_score < new_match.guest_score:
                        calculate_points(new_match.guest_id, POINTS_FOR["victory"])
                        calculate_points(new_match.home_id, POINTS_FOR["defeat"])
                        new_match.winner = 2
                    else:
                        calculate_points(new_match.guest_id, POINTS_FOR["draw"])
                        calculate_points(new_match.home_id, POINTS_FOR["draw"])
                        new_match.winner = 0
                    db.add(new_match)
                    return render_template("match_form.html", teams=teams, message="Match added to database")
                return render_template("match_form.html", teams=teams, message="Score can't be negative")
            return render_template("match_form.html", teams=teams, message="The teams has to be different")
        else:
            return render_template("match_form.html", teams=teams, message="The form is missing some values")

    #GET branch
    return render_template("match_form.html", teams=teams)

@app.route("/deleteTeam/<id>", methods=["GET", "POST"])
def delete_team(id):
    team = db.query(Team).filter(Team.id == id, Team.ACTIVE == 1).one_or_none()
    if team:
        team.ACTIVE = 0
        db.commit()
        return "Team has been deactivated"
    return "Team not found"

@app.route("/modifyTeam/<id>", methods=["GET", "POST"])
def modify_team(id):
    team = db.query(Team).filter(Team.id == id, Team.ACTIVE == 1).one_or_none()
    if request.method == "POST":
        if request.form["team_name"]:
            team.name = request.form["team_name"]
            if request.form["captain"]:
                team.captain = request.form["captain"]
            if request.form["founded"]:
                team.founded = request.form["founded"]
            db.commit()
            return render_template("team_form.html", team=team, message="Changes saved!")
        return render_template("team_form.html", team=team, message="Team name is mandatory!")
   
    if team:
        return render_template("team_form.html", team=team)
    return "Team not found"

@app.route("/modifyMatch/<id>", methods=["GET", "POST"])
def modify_match(id):
    teams = db.query(Team).filter(Team.ACTIVE == 1).order_by(Team.name).all()
    match = db.query(Match).filter(Match.id == id).one_or_none()
    if match:
        if request.method == "POST":
            if request.form["home_team"] and request.form["guest_team"] and request.form["home_score"] and request.form["guest_score"]:
                if request.form["home_team"] != request.form["guest_team"]:
                    if int(request.form["home_score"]) >= 0 and int(request.form["guest_score"]) >= 0:
                        reset_match_results(match)
                        match.home_id = request.form["home_team"]
                        match.guest_id = request.form["guest_team"]
                        match.home_score = request.form["home_score"]
                        match.guest_score = request.form["guest_score"]

                        #Determining winner and calculating scores
                        if match.home_score > match.guest_score:
                            calculate_points(match.home_id, POINTS_FOR["victory"])
                            calculate_points(match.guest_id, POINTS_FOR["defeat"])
                            match.winner = 1

                        elif match.home_score < match.guest_score:
                            calculate_points(match.guest_id, POINTS_FOR["victory"])
                            calculate_points(match.home_id, POINTS_FOR["defeat"])
                            match.winner = 2
                        else:
                            calculate_points(match.guest_id, POINTS_FOR["draw"])
                            calculate_points(match.home_id, POINTS_FOR["draw"])
                            match.winner = 0

                        db.commit()
                        return render_template("match_form.html", match=match, teams=teams, message="Match was updated")
            return render_template("match_form.html", match=match, teams=teams, message="Invalid update!")

        return render_template("match_form.html", match=match, teams=teams)
    return "No match found"


if __name__ == "__main__":
    app.run(debug=True)