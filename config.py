#This dict fhol the amount of point teams get for win, lose and draw
#This will always be added to the actual score
POINTS_FOR = {
    'victory' : 3,
    'defeat' : 0,
    'draw' : 1
}