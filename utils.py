from models import *
from config import *

def calculate_points(team_id, points):
    team = db.query(Team).filter(Team.id == team_id).one_or_none()
    if team:
        team.points += points
        db.commit()

def reset_match_results(match_obj):
    #Redoing previous results
    if match_obj.winner == 1:
        calculate_points(match_obj.home_id, POINTS_FOR["victory"]*-1)
        calculate_points(match_obj.guest_id, POINTS_FOR["defeat"]*-1)

    elif match_obj.winner == 2:
        calculate_points(match_obj.guest_id, POINTS_FOR["victory"]*-1)
        calculate_points(match_obj.home_id, POINTS_FOR["defeat"]*-1)
    else:
        calculate_points(match_obj.guest_id, POINTS_FOR["draw"]*-1)
        calculate_points(match_obj.home_id, POINTS_FOR["draw"]*-1)