from models import *
from random import sample, randint, choice
from utils import *

APLHABET = "abcdefghijklmnopqrstuvwxyz"

def generate_teams(x):
    for i in range(x):
        new_team = Team(
            name = ''.join(sample(APLHABET, 3)).upper(),
            founded = 1900 + randint(0,80),
        )

        if randint(0,2) == 1:
            new_team.captain = "Vanneki Pénter"

        db.add(new_team)
    db.commit()
generate_teams(5)

def generate_match(x):
    ids =  [x[0] for x in db.query(Team.id).all()]
    print(ids)
    for i in range(x):
        new_match = Match(
            home_id = choice(ids),
            guest_id = choice(ids),
            home_score = randint(0,5),
            guest_score = randint(0,5),
        )

        if new_match.home_score > new_match.guest_score:
            calculate_points(new_match.home_id, POINTS_FOR['victory'])
            calculate_points(new_match.guest_id, POINTS_FOR['defeat'])
            new_match.winner = 1

        elif new_match.home_score < new_match.guest_score:
            calculate_points(new_match.guest_id, POINTS_FOR['victory'])
            calculate_points(new_match.home_id, POINTS_FOR['defeat'])
            new_match.winner = 2
        else:
            calculate_points(new_match.guest_id, POINTS_FOR['draw'])
            calculate_points(new_match.home_id, POINTS_FOR['draw'])
            new_match.winner = 0
        db.add(new_match)
    db.commit()
#generate_match(20)