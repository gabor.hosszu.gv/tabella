from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, create_engine, or_
from sqlalchemy.orm import sessionmaker, relationship
import datetime

Base = declarative_base()

class Team(Base):
    __tablename__ = "teams"
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, nullable=False)
    captain = Column(String)
    founded = Column(Integer)
    points = Column(Integer, nullable=False, default=0)
    ACTIVE = Column(Integer, nullable=False, default=1)
    CREATEDON = Column(String, nullable=False, default=datetime.datetime.now)
    UPDATEON = Column(String, onupdate=datetime.datetime.now)

class Match(Base):
    __tablename__ = "matches"
    id = Column(Integer, primary_key=True)
    home_id = Column(ForeignKey("teams.id"), nullable=False)
    guest_id = Column(ForeignKey("teams.id"), nullable=False)
    home_score = Column(Integer, nullable=False)
    guest_score = Column(Integer, nullable=False)
    winner = Column(Integer, nullable=False)
    date = Column(String)
    ACTIVE = Column(Integer, nullable=False, default=1)
    CREATEDON = Column(String, nullable=False, default=datetime.datetime.now)
    UPDATEON = Column(String, onupdate=datetime.datetime.now)

    home_team = relationship(Team, foreign_keys=[home_id])
    guest_team = relationship(Team, foreign_keys=[guest_id])

engine = create_engine('sqlite:///database.db', connect_args={'check_same_thread': False})
Base.metadata.create_all(bind=engine)

Session = sessionmaker(bind=engine)

db = Session()