Követelmény:
- Csapatok kezelése (rögzítése, módosítása és törlése)
- Meccs eredmények rögzítése és módosítani
- Tabella, ahol látszik a csapatok ranglistája



Funkcionalitás:
- Csapatok:
    - Csapatok létrehozása
    - Csapatok megtekintése
        - Csapat adatai
        - Csapat korábbi meccsei
    - Csapat adatainak módosítása 
    - Csapat törlése

- Meccsek rögzítése
    - Hazai csapat
    - Vendég csapat
    - Haza csapat meccsen szerzett pontjai
    - Vendég csapat meccsen szerzett pontjai
    - Frissítse a ranglistát
- Összes meccs listázása

- Tabella, ami listázza a csapatok helyezését és pontszámait


Stack:
- Flask
- SQLalcemy
- Bootstrap
- (jQuery)


Adatbázis:
- Csapatok (teams) Tábla:
    - *id: int, primary key
    - *name: text
    - captain: text
    - founded: int
    - *points: int : 0
    - *ACTIVE: int : 1
    - *CREATEDON: text
    - UPDATEON:  text

- Meccsek (matches) tábla:
    - *id: int, primary
    - *home_id: int : foreign key (teams.id)
    - *guest_id: int: foreign key (teams.id)
    - *home_score: int
    - *guest_score: int
    - *winner: int (1 = home; 2 = guest, 0 = draw)
    - date: int
    - *ACTIVE: int : 1
    - *CREATEDON: text
    - UPDATEON:  text


Endpoint-ok:
- '\' : Tabella

- '\teams\new' : * Új csapat rögzítése
- '\teams\<id>' : Csapat adatai és korábbi meccsei
- '\modifyTeams\<id>' : * Csapat módosítása vagy törlése

- '\matches\' : Összes meccs listázása
- '\matches\new' : * Meccs rögzítése
- '\modifyMatches\<id>' : * Meccsek módosítása vagy törlése


* Jogosultságok:
- Csak admin (nginx-ben konfigurált):
    - CUD műveletek


Nice to have features:
- exportálás
- adatbetöltés
- API itegráció (más site-ról beszippant adatokat)
- külön ligák létrehozása
- meccsen szerzett  / kapott pontok